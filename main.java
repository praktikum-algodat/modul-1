public class main {
    /*
    class dibawah merupakan class main yang dimana class yang berfungsi mengeksekusi program ketika dijalankan        
    */
    public static void main(String[] args) {
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton1 yang nanti akan digunakan 
        */
        Node penonton1 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton2 yang nanti akan digunakan 
        */
        Node penonton2 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton3 yang nanti akan digunakan 
        */
        Node penonton3 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton4 yang nanti akan digunakan 
        */
        Node penonton4 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton5 yang nanti akan digunakan 
        */
        Node penonton5 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton6 yang nanti akan digunakan 
        */
        Node penonton6 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton7 yang nanti akan digunakan 
        */
        Node penonton7 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton8 yang nanti akan digunakan 
        */
        Node penonton8 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton9 yang nanti akan digunakan 
        */
        Node penonton9 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton10 yang nanti akan digunakan 
        */
        Node penonton10 = new Node();
        /*
        kode di bawah ini adalah inisialisasi objek dengan tipe data node dengan objek penonton11 yang nanti akan digunakan 
        */
        Node penonton11 = new Node();

        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton1. 
        */
        penonton1.SetData("Alita", 155, 33);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton2. 
        */
        penonton2.SetData("Putri", 158, 34);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton3. 
        */
        penonton3.SetData("Devian", 160, 36);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton4. 
        */
        penonton4.SetData("Melki", 165, 40);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton5. 
        */
        penonton5.SetData("Pande", 168, 51);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton6. 
        */
        penonton6.SetData("Azzam", 170, 57);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton7. 
        */
        penonton7.SetData("Weng", 182, 61);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton8. 
        */
        penonton8.SetData("Jidan", 183, 62);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton9. 
        */
        penonton9.SetData("Dadi", 158, 37);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton10. 
        */
        penonton10.SetData("Agus", 169, 54);
        /*
        SetData berfungsi dalam menmasukkan objek yang ada pada function menjadi string nama, integer tinggi, dan integer power yang dimasukkan dalam fungsi tersebut ketika dipanggil
        yang diinisialisasikan menggunakan fungsi penonton11. 
        */ 
        penonton11.SetData("Permadi", 0, 0);

        /*
        Inisialisasi dari Double dengan tipe data Double dan objek Doubly yang nantinya akan dipanggil
        dengan fungsi-fungsi pada class Double.
        */
        Double Doubly = new Double();
        /*
        Doubly.insertfirst berfungsi untuk menambahkan objek node yang dimulai dari head pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton1
        */
        Doubly.insertfirst(penonton1);
        /*
        Doubly.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton2
        */
        Doubly.insertLast(penonton2);
        /*
        Doubly.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton3
        */
        Doubly.insertLast(penonton3);
        /*
        Doubly.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton4
        */
        Doubly.insertLast(penonton4);
        /*
        Doubly.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton5
        */
        Doubly.insertLast(penonton5);
        /*
        Doubly.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton6
        */
        Doubly.insertLast(penonton6);
        /*
        Doubly.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton7
        */
        Doubly.insertLast(penonton7);
        /*
        Doubly.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton8
        */
        Doubly.insertLast(penonton8);
        /*kode dibawah berfungsi untuk menampilkan tulisan NOMOR 1*/
        System.out.println("NOMOR 1");
        /*kode dibawah berfungsi untuk menampilkan tulisan KETIKA PENAMPILAN FEST*/
        System.out.println("KETIKA PENAMPILAN FEST");
        /*
        Doubly.display berfungsi untuk mencetak objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Doubly.display();
        /*kode dibawah berfungsi untuk memberikan jarak garis baru*/
        System.out.println("\n");
        /*kode dibawah berfungsi untuk menampilkan tulisan KETIKA DEVIAN KELUAR BARISAN*/
        System.out.println("KETIKA DEVIAN KELUAR BARISAN");
        /*
        Doubly.insertLast berfungsi untuk menghapus objek di p3 pada link list yang telah dibuat pada class Double
        */
        Doubly.delete(penonton3);
        /*
        Doubly.display berfungsi untuk mencetak objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Doubly.display();
        /*kode dibawah berfungsi untuk memberikan jarak garis baru*/
        System.out.println("\n");
        /*kode dibawah berfungsi untuk menampilkan tulisan SETELAH DADI GANTIKAN DEVIAN*/
        System.out.println("SETELAH DADI GANTIKAN DEVIAN");
        /*
        Doubly.insertNode berfungsi untuk menambahkan objek node p9 ke dalam output terminal pada posisi setelah p2. Fungsi ini
        terdapat dalam class Double.
        */
        Doubly.insertNode(penonton2, penonton9);
        /*
        Doubly.display berfungsi untuk mencetak objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Doubly.display();
        /*kode dibawah berfungsi untuk memberikan jarak garis baru*/
        System.out.println("\n");
        /*kode dibawah berfungsi untuk menampilkan tulisan KETIKA AZZAM KELUAR BARISAN*/
        System.out.println("KETIKA AZZAM KELUAR BARISAN");
                /*
        Doubly.insertLast berfungsi untuk menghapus objek di penonton6 pada link list yang telah dibuat pada class Double
        */
        Doubly.delete(penonton6);
        /*
        Doubly.display berfungsi untuk mencetak objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Doubly.display();
        /*kode dibawah berfungsi untuk memberikan jarak garis baru*/
        System.out.println("\n");
        /*kode dibawah berfungsi untuk menampilkan tulisan SETELAH AGUS GANTIKAN AZAM*/
        System.out.println("SETELAH AGUS GANTIKAN AZAM");
        /*
        Doubly.insertNode berfungsi untuk menambahkan objek node p10 ke dalam output terminal pada posisi setelah p5. Fungsi ini
        terdapat dalam class Double.
        */
        Doubly.insertNode(penonton5, penonton10);
        /*
        Doubly.display berfungsi untuk mencetak objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Doubly.display();

        /*kode dibawah berfungsi untuk menampilkan tulisan NOMOR 2*/
        System.out.println("\nNOMOR 2");
        /*kode dibawah berfungsi untuk menampilkan tulisan "BERTEDUH*/
        System.out.println("BERTEDUH");
        /*
        Inisialisasi dari Double dengan tipe data Double dan objek Berteduh yang nantinya akan dipanggil
        dengan fungsi-fungsi pada class Double.
        */
        Double Berteduh = new Double();
        /*
        Berteduh.insertFirst berfungsi untuk menambahkan objek node yang dimulai dari head pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek p9
        */
        Berteduh.insertfirst(penonton9);
        /*
        Berteduh.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek p10
        */
        Berteduh.insertLast(penonton10);
        /*
        Berteduh.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton7
        */
        Berteduh.insertLast(penonton7);
        /*
        Berteduh.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek penonton8
        */
        Berteduh.insertLast(penonton8);
        /*
        Berteduh.display2 berfungsi untuk mencetak objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Berteduh.display2();
        /*kode dibawah berfungsi untuk memberikan jarak garis baru*/
        System.out.println("\n");
        /*
        Inisialisasi dari Double dengan tipe data Double dan objek Kehujanan yang nantinya akan dipanggil
        dengan fungsi-fungsi pada class Double.
        */
        Double Kehujanan = new Double();
        /*kode dibawah berfungsi untuk menampilkan tulisan TETAP MENONTON KONSER*/
        System.out.println("\nTETAP MENONTON KONSER");
        /*
        Kehujanan.insertFirst berfungsi untuk menambahkan objek node yang dimulai dari head pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek p1
        */
        Kehujanan.insertfirst(penonton1);
        /*
        Kehujanan.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek p2
        */
        Kehujanan.insertLast(penonton2);
        /*
        Kehujanan.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek p4
        */
        Kehujanan.insertLast(penonton4);
        /*
        Kehujanan.insertLast berfungsi untuk menambahkan objek node yang dimulai dari tail pada link list yang telah dibuat 
        pada class Double yang ditambahkan adalah ke dalam objek p11
        */
        Kehujanan.insertLast(penonton11);
        /*
        Berteduh.display2 berfungsi untuk mencetak objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Kehujanan.display2();
        /*kode dibawah berfungsi untuk memberikan jarak garis baru*/
        System.out.println("\n");

        /*kode dibawah berfungsi untuk menampilkan tulisan OUTPUT*/
        System.out.println("\nOUTPUT");
        /*kode di bawah ini adalah kode yang berfungsi untuk menunjuk 
        current pada head pada linkedlist berteduh yang mana head dari
        linked list berteduh adalah dadi*/
        Berteduh.setcurr(Berteduh.getHead());
        /*kode di bawah ini adalah kode yang berfungsi untuk memindahkan
        pointer head ke objek setelah head itu sendiri, sehingga yang 
        tadinya dadi sebagai head, sekarang Agus lah yang akan menjadi 
        head pada linked list, current akan tetap menunjuk pada Dadi*/
        Berteduh.setHead(Berteduh.getHead().getNextNode());
        /*kode di bawah ini adalah kode yang berfungsi untuk menyambungkan
        tail dari linked list ujan ke head dari linked list berteduh.Berarti 
        menyambungkan Permadi pada linkedllist ujan dengan Agus pada linkedlist 
        berteduh*/
        Kehujanan.getTail().setNext(Berteduh.getHead());
        /*kode di bawah ini adalah kode yang berfungsi untuk menyambungkan 
        objek setelah head dari linked list ujan (Putri) dengan objek current
        pada linkedlist berteduh (Dadi)*/
        Kehujanan.insertNode(Kehujanan.getHead().getNextNode(), Berteduh.getCurrent());
        /*
        Kehujanan.display2 berfungsi untuk mencetak data dari objek node yang telah ditambahkan dalam output terminal. Fungsi ini
        terdapat dalam class Double.
        */
        Kehujanan.display2();
    }
}
