/*Membuat class Node yang bersifat public dimana akan berfungsi sebagai wadah
untuk menampung berbagai method yang akan dieksekusi oleh program nantinya*/
public class Node {
    /*Kode prevNode dan nextNode bawah ini berfungsi sebagai penanda pada Double linklist jika kode sebelum dan selanjutnya  bernilai null dan nilai null
    akan berubah sesuai kebutuhan dari function*/
    public Node prevNode, nextNode=null;
    /*kode dibawah berfungsi untuk menyimpan variabel data dalam bentuk String yang nantinya akan menyimpan data nama penonton konser*/
    private String Id = "";
    /*kode dibawah berfungsi untuk menyimpan variabel data dalam bentuk Integer yang nantinya
    akan menyimpan data tinggi penonton dan kekuatan penonton konser tetapi kode dibawah hanya bisa diakses oleh didalam class Node saja*/
    private int height, power;

    /*function dibawah berfungsi sebagai function untuk menampung data yang akan di input sesuai dengan tipe jenis data
    yaitu id untuk menampung nama, height untuk menampung tinggi badan dan power untuk menampung kekuatan*/
    public void SetData(String Id, int height, int power){
        /*kode this id berfungsi untuk menyimpan data nama yang dimana data ini juga menunjukkan nilai id dari node yang sekarang*/
        this.Id=Id;
        /*kode this height berfungsi untuk menyimpan data tinggi badan yang dimana data ini juga menunjukkan nilai height dari node yang sekarang*/
        this.height=height;
        /*kode this power berfungsi untuk menyimpan data kekuatan yang dimana data ini juga menunjukkan nilai power dari node yang sekarang*/
        this.power=power;
    }
    /*function dibawah berfungsi untuk mengambil nilai id yang di mana getId tersebut adalah untuk nama yang akan dimasukkan*/
    public String getId() {
        /*kode this id berfungsi untuk menyimpan data nama yang dimana data ini juga menunjuk nilai id dari node yang sekarang*/
        return this.Id;
    }
    /*function dibawah berfungsi untuk mengambil nilai berat badan penonton yang di mana getHeight tersebut merupakan function untuk memasukkan nilainya.*/
    public int getHeight() {
        /*kode this height berfungsi untuk menyimpan data tinggi badan yang dimana data ini juga menunjuk nilai height dari node yang sekarang*/
        return this.height;
    }
    /*function dibawah berfungsi untuk mengambil nilai kekuatan badan penonton yang di mana getPower merupakan function untuk memasukkan nilainya*/
    public int getPower() {
        /*kode this power berfungsi untuk menyimpan data kekuatan yang dimana data ini juga merupakan nilai power dari node yang sekarang*/
        return this.power;
    }
    // function setNextNode berfungsi berfungsi untuk meninputkna data node selanjutnya dari node sekarang
    public void setNext(Node a){
        /*kode dibawah berfungsi untuk menyimpan data node a yang dimana data ini juga menunjuk nilai dari setNextNode dari node a yang sekarang*/
        this.nextNode = a;
    }
    /*function dibawah berfungsi untuk mengambil data node yang ada di node selanjutnya*/
    public Node getNextNode() {
        return this.nextNode;
    }
    /*function dibawah berfungsi untuk meninput data node yang ada di node sebelumnya*/
    public void setPrevNode(Node prevNode) {
        /*kode dibawah berfungsi untuk menyimpan data node prevNode yang dimana data ini juga menunjuk nilai dari setPrevNode dari node prevNode yang sekarang*/
        this.prevNode = prevNode;
    }
    /*function dibawah berfungsi untuk mengambil data node yang ada di node sebelumnya*/
    public Node getPrevNode() {
        /*kode dibawah berfungsi untuk mengambil data sebelumnya yang sekarang*/
        return this.prevNode;
    }
    //function ini berfungsi untuk melakukan pengecekan terhadap node apakah node tersebut adalah list node yang terakhir terakhir 
    public boolean isTail() {
        /*  perulangan ini berfungsi untuk mengecek apakah nilai node selanjutya yang sekrang sama dengan null atau tidak, 
        jika nilainya sama maka akan mereturn nilai true*/
        if (this.nextNode == null)
            return true;
        // dan jika tidak akan mereturn nilai false
        return false;
    }
}