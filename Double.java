public class Double {
    /*
     * Node head, tail, cur adalah data dari doublyLinkedList yang bertipe data
     * Node yang akan digunakan dalam program selanjutnya.
     * Head disini merupakan node paling kiri atau node pertama
     * Tail disini merupakan node paling kanan atau node terakhir
     * Cur disini merupakan penyimpan sementara node sekarang
     */
    Node head;
    Node tail;
    Node cur;

    // Fungsi setcurr adalah fungsi untuk menyimpan nilai input yang bertipe data
    // Node
    public void setcurr(Node input) {
        // this.cur disini digunakan untuk menyimpan cur yang sekarang sebagai input
        // atau node yang akan disimpan
        this.cur = input;
    }

    // Fungsi getCurrent digunakan untuk mengambil Node
    public Node getCurrent() {
        // nilai return cur adalah pengembalian nilai bertipe data node yaitu cur
        return cur;
    }

    // Fungsi getTail digunakan mengambil tail dari Node
    public Node getTail() {
        // nilai return tail adalah pengembalian nilai bertipe data node yaitu tail
        return tail;
    }

    // Fungsi getHead digunakan mengambil head dari Node
    public Node getHead() {
        return head;
    }

    // Fungsi setHead adalah fungsi untuk menyimpan head yang bertipe data Node.
    // fungsi ini juga digunakan untuk
    // menjadikan Node head sebagai head atau node pertama
    public void setHead(Node head) {
        // this.head digunakan untuk menjadikan head menjadi node pertama atau paling
        // kiri
        this.head = head;
    }

    // Fungsi boolean isEmpty adalah fungsi yang digunakan untuk pengecekan apak isi
    // dari list Node kosong atau tidak
    // dengan cara mengecek apakah terdapat Node pada head;
    public boolean isEmpty() {
        // return this.head adalah null pengecekan head kosong atau tidak
        return this.head == null;
    }

    // Fungsi insertfirst adalah fungsi yang digunakan untuk menambahkan node
    // kedalam list pertama atau head
    public void insertfirst(Node insert) {
        /* kondisi if(isEmpty()) adalah pengecekan apakah list kosong atau tidak jika
           list kosong maka Node insert
           akan ditambahkan langsung di dalam list*/
        if (isEmpty()) {
            // tail = insert digunakan untuk menyimpan Node insert pada list
            tail = insert;
            // kondisi else disini digunakan untuk menjalankan code apabila kondisi diatas
            // tidak terpenuhi
        } else {
            // head.prevNode= insert digunakan untuk menyimpan Node sebelumnya dari Node
            // head
            head.prevNode = insert;
        }
        // insert.nextNode= head digunakan untuk menjadikan head sebagai node
        // selanjutnya dari insert
        insert.nextNode = head;
        // head = insert digunakan untuk menjadikan insert sebgai head atau node pertama
        head = insert;
    }

    // fungsi insertlast adallah fungsi untuk menambahkan node pada list di bagian
    // akhir atau tail
    public void insertLast(Node Insert) {
        // kondisi if(isEmpty()) adalah pengecekan apakah list kosong atau tidak jika
        // list kosong maka Node insert
        // akan ditambahkan langsung di dalam list
        if (isEmpty())
            // proses head = fungsi digunakan untuk langsung menambahkan Node Insert ke list
            // pertama
            head = Insert;
        // kondisi else disini digunakan untuk menjalankan code apabila kondisi diatas
        // tidak terpenuhi
        else {
            // tail.nextNode = Insert digunakan untuk menjadikan Insert sebgai tail dari
            // Node sekarang
            tail.nextNode = Insert;
            // Insert.prevNode = tail digunakan untuk menjadikan tail dari Node sekarang
            // menjadi previous
            // Node atau Node sebelumnya dari insert
            Insert.prevNode = tail;
        }
        // tail= insert digunakan untuk menjadikan tail sebagai Node insert
        tail = Insert;
    }

    // fungsi insertNode adalah fungsi yang digunakan untuk memasukan Node setelah
    // node yang dipillih
    public void insertNode(Node pos, Node insert) {
        // Node temp dan prevtemp adalah pendeklarasian variabel yang bertipe data Node
        Node temp;
        Node prevtemp;
        // Node cur adalah pendeklarian variabel cur bertipe data node yang diisi oleh
        // node pertama dari list
        Node cur = head;
        // kondisi if pos==null adalah kondisi yang dijalankan apabila Node pos adalah
        // null;
        if (pos == null) {
            // insertfirst (insert) adalah proses penambahan Node insert ke List Node
            // pertama
            insertfirst(insert);
            // else disini dijalnkan apabila kondisi di atas tidak terpenuhi
        } else {
            // proses yang dijalnkan adalah perulangan do yang dimana dijalankan apabila
            // kondisi whilenya terpenuhi
            do {
                /* kondisi cur.equals(pos) adalah kondisi jika Node cur sama dengan Node pos
                maka proses selanjutnya di jalankan*/
                if (cur.equals(pos)) {
                    // temp = pos.nextNode adalah proses menyimpan Node selanjutnya dari Node pos
                    // yang disimpan di temp
                    temp = pos.nextNode;
                    // prevtemp = pos.nextNode.getPrevNode() adalah proses untuk menyimpan Node
                    // sebelumnya dari Node selanjutnya pada
                    // Node pos yang disimpan di prevtemp
                    prevtemp = pos.nextNode.getPrevNode();
                    // pos.nextNode = insert adalah proses mengganti node selanjutnya dari pos
                    // menjadi Node insert
                    pos.nextNode = insert;
                    // insert.nextNode = temp adalah proses mengganti nextNode dari Insert menjadi
                    // Node temp yang telah disimpan Node
                    // selanjutnya dari Node pos
                    insert.nextNode = temp;
                    // insert.prevNode=prevtemp adalah proses yang mengganti Node sebelumnya dari
                    // insert menjadi prevtemp 
                    insert.prevNode = prevtemp;
                    // break adalah proses memberhentikan perulangan
                    break;
                    // kondisi else dibawah dijalankan apabila kondisi diatas tidak terpenuhi
                } else {
                    // proses yang dijalankan adalah cur=cur.nextNode adalah proses menyimpan nilai
                    // cur menjadi Node selanjutnya dari Node cur
                    cur = cur.nextNode;
                }
                // while (cur !=null) atau nilai sekarang tidak kosong adalah kondisi yang perlu
                // terpenuhi jika ingin melakukan perulangan do diatas
            } while (cur != null);
        }
    }

    // fungsi delete adalah fungsi yang digunakan untuk menghapus Node yang ingin
    // dihapus
    public void delete(Node del) {
        // Node cur = head digunakan untuk menyimpan head dari node sekarang ke cur
        Node cur = head;
        // do adalah perulangan
        do {
            // cur = cur.nextNode adalah operasi yang digunakan untuk mengambil Node
            // selanjutnya
            // dari cut dan disimpan di cur
            cur = cur.nextNode;
            // kondisi if disini digunakan untuk pengecekan apabila Node cur kosong makan
            // dia akan kembali
            if (cur == null)
                return;
            // while (cur != del) adalah kondisi yang harus terpenuhi untuk perulangan do
            // diatas jika Node cur sama dengan Node del makan proses akan berhenti
        } while (cur != del);
        // kondisi if cur==head adalah kondisi pengecekan apakah Node cur sama dengan
        // head
        if (cur == head)
            // head = cur.nextNode adalah operasi jika kondisi diatas terpenuhi dimana
            // menjadikan head sebagai nilai selanjutnya dari node sekarang
            head = cur.nextNode;
        // else adalah kondisi jika kondisi diatas tidak terpenuhi
        else
            // proses yang dijalankan adalah cur.prevNode.nextNode = cur.nextNode yang
            // dimana proses ini menjadikan node node selanjutnya dari node sekarang
            cur = cur.nextNode;
        // kondisi (cur.equals(tail)) adalah kondisi mengecek apakah Node sekarang sama
        // dengan Node terakhir
        if (cur.equals(tail))
            // proses tail = cur.prevNode adalah proses yang dijalankan aoabila kondisi
            // diatas terpenuhi proses
            // ini menjadikan tail dari list menjadi node sebelumnya dari Node sekarang
            tail = cur.prevNode;
        // else adalah kondisi jika kondisi diatas tidak terpenuhi
        else
            // proses yang dijalankan adalah cur = cur.prevNode adalah proses menyimpan
            // nilai sebelumnya dari Node sekarang
            // ke Node sekarang
            cur = cur.prevNode;
    }

    // Fungsi display adalah fungsi yang digunakan untuk mencetak List dari Node
    // pada program
    public void display() {
        // Node p adalah deklari variabel p bertipe data Node
        Node p;
        // p=this.head adalah proses untuk menjadikan Node p sebagai Node Head dari list
        p = this.head;
        // perulangan do adalkah perulangan yang dilakukan jika suatu kondisi terpenuhi
        do {
            // proses di bawah adalah proses yang dilakukan jika kondisi while terpenuhi
            // proses ini adalah mencetak semua
            // data dari Node yaitu nama, height, dan power.
            System.out.println("Nama : " + p.getId() + ",\tHeight : " + p.getHeight() + ",\tPower : " + p.getPower());
            // proses selanjutnya adalah proses menyimpan nilai selanjutnya dari Node p
            p = p.getNextNode();
            // kondisi while (!p.isTail()) adalah kondisi yang harus terpenuhi jika ingin
            // melakukan perulangan do di atas
        } while (!p.isTail());
        // proses ini adalah proses mencetak semua data dari Node yaitu nama
        System.out.println("Nama : " + p.getId() + ",\tHeight : " + p.getHeight() + ",\tPower : " + p.getPower());
    }

    // Fungsi display adalah fungsi yang digunakan untuk mencetak List dari Node
    // pada program
    public void display2() {
        // Node p adalah deklari variabel p bertipe data Node
        Node p;
        // p=this.head adalah proses untuk menjadikan Node p sebagai Node Head dari list
        p = this.head;
        // perulangan do adalkah perulangan yang dilakukan jika suatu kondisi terpenuhi
        do {
            /* proses di bawah adalah proses yang dilakukan jika kondisi while terpenuhi
            proses ini adalah mencetak semua
            data dari Node yaitu nama*/
            System.out.print(p.getId() + "=>");
            // proses selanjutnya adalah proses menyimpan nilai selanjutnya dari Node p
            p = p.getNextNode();
            // kondisi while (!p.isTail()) adalah kondisi yang harus terpenuhi jika ingin
            // melakukan perulangan do di atas
        } while (!p.isTail());
        // proses ini adalah proses mencetak semua data dari Node yaitu nama
        System.out.print(p.getId());
    }
}
